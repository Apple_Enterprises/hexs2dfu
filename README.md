# Hexs2Dfu
**Hexs2Dfu** is a light weight Windows console utility written in C++ that combines one or more HEX files into a single ST DFU file. It is meant to be used as a build action after compiling multiple firmware projects.  

## Motivation
**Hexs2Dfu** is useful when you have several pieces of firmware that you want to keep
together. As an example, consider a motherboard that has several different cards that
plug into it. The motherboard and the plug-in cards each have their own firmware. A
programming application could read the DFU file and program the appropriate card or motherboard from a single DFU file. The programming application could also program all
the firmware into the motherboard and have the motherboard program each of the plug-in
cards. In any event all the firmware is in one place so that versioning issues between
each of the pieces is alleviated.


There are three essential files:  
- IntelHexRecords: functions to read hex files  
- DfuFile: functions to write a DfuFile  
- Hexs2Dfu: logic to read from hex files and write to DFU file  

Hexs2Dfu.cpp can be modified for firmware to read a DFU file and program the flash for a firmware upgrade.

## Installation

Copy Hexs2Dfu.exe to your desired directory. When Hexs2Dfu starts it sets the working directory to the directory where Hexs2Dfu.exe resides.

## Usage

Hexs2Dfu [-x parm] [-x parm] ...

where x is one of the following options and parm is the parameter associated with the option.
The options can be placed in an options text file instead of on the command line.
```sh
-b - firmware version number (optional, default: 0xFFFF))
-f - file name containing options (optional)
-h - help (no parameter)
-i - input HEX file name (mandatory))
-n - input name (optional, default: PCB x))
-o - output DFU file name (mandatory))
-O - offset to be added to firmware address (optional)
-p - USB Pid (optional, default: 0xDF11))
-v - USB Vid (optional, default: 0x0483))
```
The firmware address offset is added to the input file starting address to provide relocation. Useful to place firmware for a separate PCB at a known empty firmware location for later programming by the firmware. 
## Usage examples

### Example 1
Creates a DFU file using a single HEX file.
```sh
Hexs2Dfu -i infile.hex -o outfile.dfu  
```

### Example 2
Combines four HEX files into a DFU file using an option file.
```sh
Hexs2Dfu -f DfuConfig.cfg
```
Contents of DfuConfig.cfg
```sh
-i "..\TestFiles\Bootloader.hex" -n "Bootloader" -O 0
-i "..\TestFiles\Main.hex" -n "Main PCB" -O 0
-i "..\TestFiles\Test1.hex" -n "PCB 1" -O 0x10000
-i "..\TestFiles\Test2.hex" -n "PCB 2" -O 0x20000
-o "..\TestFiles\TestResult.dfu"
-b 0x000e
-p 0xdf11
-v 0x0483
```

## Development setup

Install **Visual Studio 2019 Preview**. It should work on previous versions, but I haven't checked.  
Build the project in debug mode. 
The debug parameters are set to: 
```sh
-f "..\DfuConfig.cfg" 
```
Start debugging. You will see a CMD window pop up with the result.
## Release History

* 0.1.0
    * The first proper release
    * README.md created online with Bitbucket

## Meta

Chris Apple � [@SaratogaApple](https://twitter.com/SaratogaApple) � capple@applehome.com  
[http://www.applehome.com/](http://www.applehome.com/) 


Distributed under the MIT license. See ``LICENSE`` for more information.

[https://bitbucket.org/Apple_Enterprises/hexs2dfu](https://bitbucket.org/Apple_Enterprises/hexs2dfu)

## Contributing

1. Fork it (<https://bitbucket.org/Apple_Enterprises/hexs2dfu/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

