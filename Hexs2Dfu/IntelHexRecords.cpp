/*******************************************************************************
 * Copyright (c) 2019 Apple Enterprises. http://www.applehome.com/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal 
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
 * copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#include "pch.h"
#include "stdio.h"
#include "IntelHexRecords.h"


CIntelHexRecords::CIntelHexRecords()
{
}

CIntelHexRecords::~CIntelHexRecords()
{
}


bool CIntelHexRecords::OpenHexFile(LPCTSTR fileName)
{
	m_errorCode = HEX_FILE_ERROR::ERROR_NONE;

	errno_t error = _tfopen_s(&m_file, fileName, _T("rt"));
	if (error != 0) {
		return false;
	}
	return true;
}

void CIntelHexRecords::CloseHexFile()
{
	fclose(m_file);
}


bool CIntelHexRecords::ReadHexRecord()
{
	m_hexRecordLineNumber++;

	TCHAR * getResult = _fgetts(m_hexRecord, sizeof(m_hexRecord), m_file);
	if (getResult == NULL) {
		if (ferror(m_file)) {
			m_errorCode = HEX_FILE_ERROR::READ_FAIL;
			return false;
		}
		if (feof(m_file) != 0) {
			// end of file
			m_errorCode = HEX_FILE_ERROR::READ_FAIL;
			return false;
		}
		m_errorCode = HEX_FILE_ERROR::READ_FAIL;
		return false;
	}

	// strip off trailing \n
	uint32_t recordSize = _tcslen(m_hexRecord);
	if (m_hexRecord[recordSize - 1] == '\n') {
		m_hexRecord[recordSize - 1] = '\0';
	}

	return true;
}

bool CIntelHexRecords::ReadFile()
{
	// seek back to beginning of file
	int32_t seekResult = fseek(m_file, 0, SEEK_SET);
	if (seekResult != 0) {
		m_errorCode = HEX_FILE_ERROR::SEEK_FAIL;
		return false;
	}

	// read complete file and make sure all the records are valid
	// Find out how may extended address blocks
	m_numberOfExtendedLinearAddress = 0;
	m_hexRecordLineNumber = 0;
	for (;;) {
		bool validRecord = ReadHexRecord();
		if (!validRecord) {
			return false;
		}

		HEX_RECORD_NUMBER recordType = (HEX_RECORD_NUMBER)GetRecordNumber(m_hexRecord);
		switch (recordType) {
		case HEX_RECORD_NUMBER::ERROR:
			m_errorCode = HEX_FILE_ERROR::HEX_RECORD;
			return false;
		case HEX_RECORD_NUMBER::DATA:
			break;
		case HEX_RECORD_NUMBER::END_OF_FILE:
			return true;
		case HEX_RECORD_NUMBER::EXTENDED_LINEAR_ADDRESS:
			m_numberOfExtendedLinearAddress++;
			break;
		case HEX_RECORD_NUMBER::START_LINEAR_ADDRESS:
			break;
		default:
			assert(false);
			m_errorCode = HEX_FILE_ERROR::TOTAL;
			return false;
		}
	}
	return true;
}

bool CIntelHexRecords::ReadBlock(int32_t blockIndex, uint8_t * buffer, uint32_t bufferLength)
{
	assert(blockIndex < m_numberOfExtendedLinearAddress);
	assert(bufferLength == 0x10000);

	// seek back to beginning of file
	int32_t seekResult = fseek(m_file, 0, SEEK_SET);
	if (seekResult != 0) {
		m_errorCode = HEX_FILE_ERROR::SEEK_FAIL;
		return false;
	}

	int32_t numberOfExtendedLinearAddress = -1;
	m_hexRecordLineNumber = 0;
	for (;;) {
		bool validRecord = ReadHexRecord();
		if (!validRecord) {
			return false;
		}
		HEX_RECORD_NUMBER recordType = (HEX_RECORD_NUMBER)GetRecordNumber(m_hexRecord);
		switch (recordType) {
		case HEX_RECORD_NUMBER::ERROR:
			m_errorCode = HEX_FILE_ERROR::HEX_RECORD;
			return false;

		case HEX_RECORD_NUMBER::DATA:
			if (numberOfExtendedLinearAddress == blockIndex) {
				// copy data into buffer
				memcpy((uint8_t*)(buffer + m_hexRecordDataAddress), m_hexRecordData, m_hexRecordDataSize);
			}
			break;

		case HEX_RECORD_NUMBER::END_OF_FILE:
			ExamineBlock(buffer, bufferLength);
			return true;

		case HEX_RECORD_NUMBER::EXTENDED_LINEAR_ADDRESS:
			numberOfExtendedLinearAddress++;
			if (numberOfExtendedLinearAddress == blockIndex) {
				m_extendedAddress = m_hexRecordExtendedLinearAddress << 16;

				// clear the buffer
				memset(buffer, 0xff, bufferLength);

			} else if (numberOfExtendedLinearAddress > blockIndex) {
				ExamineBlock(buffer, bufferLength);
				return true;
			}
			break;

		case HEX_RECORD_NUMBER::START_LINEAR_ADDRESS:
			break;

		default:
			assert(false);
			m_errorCode = HEX_FILE_ERROR::TOTAL;
			return false;
		}
	}
	return true;
}

void CIntelHexRecords::ExamineBlock(uint8_t * buffer, int32_t bufferLength)
{
	// find minimum buffer address
	for (m_minimumOffset = 0; m_minimumOffset < bufferLength; m_minimumOffset++) {
		if (buffer[m_minimumOffset] != 0xff) {
			break;
		}
	}
	for (m_maximumOffset = bufferLength-1; m_maximumOffset >= 0; m_maximumOffset--) {
		if (buffer[m_maximumOffset] != 0xff) {
			break;
		}
	}
}

// return true if buffer is not empty
bool CIntelHexRecords::GetBlockAddresses(uint32_t &extendedAddress, uint32_t &minimumBlockOffset, uint32_t &maximumBlockOffset)
{
	extendedAddress = m_extendedAddress;
	minimumBlockOffset = m_minimumOffset;
	maximumBlockOffset = m_maximumOffset;
	return m_minimumOffset < 0x10000;
}

/*
* HexConvert
* Converts a single hex character to an unsigned char value
*
* Returns the converting value if the input character is valid
* otherwise, 0xff is returned 
*/
int CIntelHexRecords::HexConvert(TCHAR c)
{
    if ((c >= 'a') && (c <= 'f'))
        return (c -'a') + 10;
    if ((c >= 'A') && (c <= 'F'))
        return (c -'A') + 10;
    if ((c >= '0') && (c <= '9'))
        return (c - '0');
    return -1;
}

/*
* GetHexByte
* Converts the first two hex characters from the string to a byte,
* returning it as a int.
*
* For a return value of -1, one of the two characters was not a hex character
*/
int CIntelHexRecords::GetHexByte(LPCTSTR p)
{
	int nibble0;
	int nibble1;

	nibble0 = HexConvert( p[0] );
	nibble1 = HexConvert( p[1] );

	if( (nibble0 == -1) || (nibble1 == -1) )
		return -1;

	
	return( (nibble0 << 4) | nibble1 );
}
/*
* GetHexDWord
* Converts the first eight hex characters from the string to a 32-bit word,
* returning it as a int.
*
* For a return value of -1, one of the eight characters was not a hex character
*/
uint32_t CIntelHexRecords::GetHexDWord(LPCTSTR p)
{
	uint32_t nibble0 = HexConvert(p[0]);
	uint32_t nibble1 = HexConvert(p[1]);
	uint32_t nibble2 = HexConvert(p[2]);
	uint32_t nibble3 = HexConvert(p[3]);
	uint32_t nibble4 = HexConvert(p[4]);
	uint32_t nibble5 = HexConvert(p[5]);
	uint32_t nibble6 = HexConvert(p[6]);
	uint32_t nibble7 = HexConvert(p[7]);

	if ((nibble0 == -1) || (nibble1 == -1) || (nibble2 == -1) || (nibble2 == -1)
		|| (nibble4 == -1) || (nibble4 == -1) || (nibble6 == -1) || (nibble7 == -1)) {
		return -1;
	}

	uint32_t result = (nibble0 << 28) | (nibble1 << 24) | (nibble2 << 20) | (nibble3 << 16) | (nibble4 << 12) | (nibble5 << 8) | (nibble6 << 4) | nibble7;

	return result;
}
/*
* GetHexWord
* Converts the first four hex characters from the string to a 16-bit word,
* returning it as a int.
*
* For a return value of -1, one of the four characters was not a hex character
*/
int CIntelHexRecords::GetHexWord(LPCTSTR p)
{
	int nibble0;
	int nibble1;
	int nibble2;
	int nibble3;

	nibble0 = HexConvert(p[0]);
	nibble1 = HexConvert(p[1]);
	nibble2 = HexConvert(p[2]);
	nibble3 = HexConvert(p[3]);

	if ((nibble0 == -1) || (nibble1 == -1) || (nibble2 == -1) || (nibble2 == -1))
		return -1;


	return((nibble0 << 12) | (nibble1 << 8) | (nibble2 << 4) | nibble3);
}
/*
* GetHexByte
* Converts the first two hex characters from the string to a byte,
* returning it as a long.
*
* For a return value of 0x100, one of the two characters 
* is not a valid hex char; otherwise,
* the conversion is valid and the value can be safely typecasted as a uchar
*/
char CIntelHexRecords::GetChar(LPCTSTR p)
{
	int highNibble = HexConvert ( p[0] );
	int lowNibble = HexConvert ( p[1] );

	char result = (highNibble << 4) + lowNibble;
	return result;
}

/*
 * From Wikipedia
 * A record (line of text) consists of six fields (parts) that appear in order from left to right:
 *
 * 1: Start code, one character, an ASCII colon ':'.
 * 2: Byte count, two hex digits, indicating the number of bytes (hex digit pairs) in the data field. 
 *    The maximum byte count is 255 (0xFF). 16 (0x10) and 32 (0x20) are commonly used byte counts.
 * 3: Address, four hex digits, representing the 16-bit beginning memory address offset of the data. 
 *    The physical address of the data is computed by adding this offset to a previously established 
 *    base address, thus allowing memory addressing beyond the 64 kilobyte limit of 16-bit addresses. 
 *    The base address, which defaults to zero, can be changed by various types of records. Base addresses
 *    and address offsets are always expressed as big endian values.
 * 4: Record type (see record types below), two hex digits, 00 to 05, defining the meaning of the data field.
 * 5: Data, a sequence of n bytes of data, represented by 2n hex digits. Some records omit this field 
 *    (n equals zero). The meaning and interpretation of data bytes depends on the application.
 * 6: Checksum, two hex digits, a computed value that can be used to verify the record has no errors.
 *
 * RECORD TYPES:
 * 00: Data - Contains data and a 16-bit starting address for the data. The byte count specifies number 
 *            of data bytes in the record. The example shown to the right has 0B (eleven) data bytes 
 *            (61, 64, 64, 72, 65, 73, 73, 20, 67, 61, 70) located at consecutive addresses beginning at
 *            address 0010.
 * 01: End Of File - Must occur exactly once per file in the last line of the file. The data field is empty
 *            (thus byte count is 00) and the address field is typically 0000.
 * 02: Extended Segment Address - The data field contains a 16-bit segment base address (thus byte count 
 *            is always 02) compatible with 80x86 real mode addressing. The address field (typically 0000) 
 *            is ignored. The segment address from the most recent 02 record is multiplied by 16 and added 
 *            to each subsequent data record address to form the physical starting address for the data. 
 *            This allows addressing up to one megabyte of address space.
 * 03: Start Segment Address - For 80x86 processors, specifies the initial content of the CS:IP registers. 
 *            The address field is 0000, the byte count is always 04, the first two data bytes are the CS 
 *            value, the latter two are the IPvalue.
 * 04: Extended Linear Address - Allows for 32 bit addressing (up to 4GiB). The record's address field is 
 *            ignored (typically 0000) and its byte count is always 02. The two data bytes (big endian)
 *            specify the upper 16 bits of the 32 bit absolute address for all subsequent type 00 records;
 *            these upper address bits apply until the next 04 record. The absolute address for a type 00 
 *            record is formed by combining the upper 16 address bits of the most recent 04 record with the
 *            low 16 address bits of the 00 record. If a type 00 record is not preceded by any type 04 
 *            records then its upper 16 address bits default to 0000.
 * 05: Start Linear Address - The address field is 0000 (not used) and the byte count is always 04. The 
 *            four data bytes represent a 32-bit address value. In the case of 80386 and higher CPUs, this
 *            address is loaded into the EIP register.
 *
 * Tests Hex Records transferred to the module for validity and stores data in the 
 * DRAM mirror of the Flash ROM memory space.
 * 
 * The following tests are performed 
 * (see _HEX_RECORD_TYPE for return values)
 * 1.Validates starting colon
 * 2.Read the length field and verify against the input hex record
 * 3.Reads the offset field from the hex record
 * 4.Validates hex record Type field: must be type 0, 1, 4, or 5
 * 5.Converts data fields and stores the data in the allocated DRAM mirror space
 * 6.Compares calculated checksum from the hex record with the checksum field
 *
 */

HEX_RECORD_NUMBER CIntelHexRecords::GetRecordNumber(LPCTSTR hexRecord)
{
	HEX_RECORD_NUMBER hexRecordNumber = HEX_RECORD_NUMBER::ERROR;
	int 	i;
	uint32_t   flashAddress = 0;
	int   recordType = 0;
	int   addressOffset = 0;
	int   highAddressOffset = 0;
	int   calculatedChecksum = 0;
	int   recordLengthInBytes = 0;
	int   data = 0;
	int   recordCheckSum;
	LPCTSTR hexRecordIn = (LPCTSTR)hexRecord;

	/*
	* Make sure first character is a colon
	*/
	if (hexRecordIn[0] != ':') {
		assert(false);
		m_hexRecordError = HEX_RECORD_ERROR::NO_STARTING_COLON;
		return HEX_RECORD_NUMBER::ERROR;
	}

	/*
	* Make sure chars 1 and 2 contain a valid length
	*/
	recordLengthInBytes = GetHexByte(&hexRecordIn[1]);
	if (recordLengthInBytes == -1) {
		assert(false);
		m_hexRecordError = HEX_RECORD_ERROR::BAD_CHAR_IN_RECORD_LENGTH;
		return HEX_RECORD_NUMBER::ERROR;
	}

	calculatedChecksum += recordLengthInBytes;

	/*
	* Determine length & verify against the record
	* length field: number of character pairs, excluding type and length pairs
	*/
	i = _tcslen(hexRecordIn) - 11;
	if ((recordLengthInBytes * 2) != i) {
		m_hexRecordError = HEX_RECORD_ERROR::INVALID_RECORD_LENGTH;
		return HEX_RECORD_NUMBER::ERROR;
	}

	/*
	* Make sure chars 3,4,5, and 6 contain a valid offset
	*/
	addressOffset = GetHexWord(&hexRecordIn[3]);
	if (addressOffset == -1) {
		assert(false);
		m_hexRecordError = HEX_RECORD_ERROR::BAD_CHAR_IN_RECORD_OFFSET;
		return HEX_RECORD_NUMBER::ERROR;
	}

	calculatedChecksum += (addressOffset & 0xff) + ((addressOffset >> 8) & 0xff);

	/*
	* Make sure chars 7 and 8 contain a valid record type
	*/
	recordType = GetHexByte(&hexRecordIn[7]);
	if (recordType == -1) {
		assert(false);
		m_hexRecordError = HEX_RECORD_ERROR::BAD_CHAR_IN_RECORD_TYPE;
		return HEX_RECORD_NUMBER::ERROR;
	}


	calculatedChecksum += recordType;

	// get number of data bytes to read

	switch (recordType) {
	case 0:				// data record
		/*
		* Test address for valid destinations in flash memory space
		* Adjust boundaries to account for length of hex record data
		* (Length field less 4 address bytes and 1 checksum byte)
		*/
		hexRecordNumber = HEX_RECORD_NUMBER::DATA;
		m_hexRecordDataAddress = addressOffset;
		m_hexRecordDataSize = recordLengthInBytes;

		for (i = 0; i < recordLengthInBytes; i++) {
			data = GetHexByte(&hexRecordIn[9 + (i * 2)]);
			if (data == -1) {
				assert(false);
				m_hexRecordError = HEX_RECORD_ERROR::BAD_CHAR_IN_DATA;
				return HEX_RECORD_NUMBER::ERROR;
			}
			calculatedChecksum += data;
			m_hexRecordData[i] = (uint8_t)data;
		}
		break;
	case 1:			// end of file
		hexRecordNumber = HEX_RECORD_NUMBER::END_OF_FILE;
		if (recordLengthInBytes != 0) {
			assert(false);
			m_hexRecordError = HEX_RECORD_ERROR::WRONG_SIZE_RECORD_LENGTH;
			return HEX_RECORD_NUMBER::ERROR;
		}
		break;
	case 2:			// Extended Segment Address
		assert(false);		// not implemented
		m_hexRecordError = HEX_RECORD_ERROR::NOT_IMPLEMENTED_RECORD;
		return HEX_RECORD_NUMBER::ERROR;
	case 3:			// Start Segment Address
		assert(false);		// not implemented
		m_hexRecordError = HEX_RECORD_ERROR::NOT_IMPLEMENTED_RECORD;
		return HEX_RECORD_NUMBER::ERROR;
	case 4:			// Extended Linear Address Record    (32-bit format only) 
		hexRecordNumber = HEX_RECORD_NUMBER::EXTENDED_LINEAR_ADDRESS;
		if (recordLengthInBytes != 2) {
			assert(false);
			m_hexRecordError = HEX_RECORD_ERROR::WRONG_SIZE_RECORD_LENGTH;
		}
		m_hexRecordExtendedLinearAddress = GetHexWord(&hexRecordIn[9]);
		if (m_hexRecordExtendedLinearAddress == -1) {
			assert(false);
			m_hexRecordError = HEX_RECORD_ERROR::BAD_CHAR_IN_DATA;
		}

		calculatedChecksum += (m_hexRecordExtendedLinearAddress & 0xff) + ((m_hexRecordExtendedLinearAddress >> 8) & 0xff);
		break;
	case 5:			// Start Linear Address Record  (32-bit format only)
		hexRecordNumber = HEX_RECORD_NUMBER::START_LINEAR_ADDRESS;
		if (recordLengthInBytes != 4) {
			assert(false);
			m_hexRecordError = HEX_RECORD_ERROR::WRONG_SIZE_RECORD_LENGTH;
			return HEX_RECORD_NUMBER::ERROR;
		}
		m_hexRecordStartLinearAddress = GetHexDWord(&hexRecordIn[9]);
		if (m_hexRecordStartLinearAddress == -1) {
			assert(false);
			m_hexRecordError = HEX_RECORD_ERROR::BAD_CHAR_IN_DATA;
			return HEX_RECORD_NUMBER::ERROR;
		}
		for (i = 0; i < recordLengthInBytes; i++) {
			data = GetHexByte(&hexRecordIn[9 + (i * 2)]);
			calculatedChecksum += data;
		}
		break;
	default:
		break;
	}


	/*
	* Convert the checksum byte & compare to computed value
	* length field: count of character pairs less rec type & length
	* checksum is the last record pair => (4 + 2*recordLengthInBytes - 2)
	*/
	recordCheckSum = GetHexByte(&hexRecordIn[9 + 2 * recordLengthInBytes]);
	if (recordCheckSum == -1) {
		assert(false);
		m_hexRecordError = HEX_RECORD_ERROR::BAD_CHAR_IN_CHECKSUM;
		return HEX_RECORD_NUMBER::ERROR;
	}

	calculatedChecksum += recordCheckSum;
	if ((calculatedChecksum & 0xff) != 0) {
		assert(false);
		m_hexRecordError = HEX_RECORD_ERROR::NON_MATCHING_CHECKSUM;
		return HEX_RECORD_NUMBER::ERROR;
	}
	return hexRecordNumber;
}

void CIntelHexRecords::GetHexRecordErrorString( TCHAR * buffer, uint32_t bufferLength )
{
	LPCTSTR szError;

	switch (m_hexRecordError) {
	case HEX_RECORD_ERROR::NO_STARTING_COLON: szError = _T("has a missing starting colon"); break;
	case HEX_RECORD_ERROR::BAD_CHAR_IN_RECORD_LENGTH: szError = _T("has a bad hex character in record length field"); break;
	case HEX_RECORD_ERROR::BAD_CHAR_IN_RECORD_OFFSET: szError = _T("has a bad hex character in offset field"); break;
	case HEX_RECORD_ERROR::BAD_CHAR_IN_RECORD_TYPE: szError = _T("has a bad hex character in record type field"); break;
	case HEX_RECORD_ERROR::INVALID_RECORD_LENGTH: szError = _T("has a bad length value"); break;
	case HEX_RECORD_ERROR::WRONG_SIZE_RECORD_LENGTH: szError = _T("has a record length that is invalid for type of record"); break;
	case HEX_RECORD_ERROR::BAD_CHAR_IN_DATA: szError = _T("has a bad hex character in data"); break;
	case HEX_RECORD_ERROR::BAD_CHAR_IN_CHECKSUM: szError = _T("has a bad hex character in checksum"); break;
	case HEX_RECORD_ERROR::NON_MATCHING_CHECKSUM: szError = _T("has a non matching checksum value"); break;
	case HEX_RECORD_ERROR::NOT_IMPLEMENTED_RECORD: szError = _T("is not implemented"); break;
	default: szError = _T("Unknown error");
	}

	_stprintf_s(buffer, bufferLength, _T("Line %d, \"%s\", %s."), m_hexRecordLineNumber, m_hexRecord, szError );

}

void CIntelHexRecords::GetHexFileErrorString(TCHAR * buffer, uint32_t bufferLength)
{
	switch (m_errorCode) {
	case HEX_FILE_ERROR::SEEK_FAIL:
		_stprintf_s(buffer, bufferLength, _T("Line %d, Seek failed."), m_hexRecordLineNumber);
		break;
	case HEX_FILE_ERROR::READ_FAIL:
		_stprintf_s(buffer, bufferLength, _T("Line %d, Read failed."), m_hexRecordLineNumber);
		break;
	case HEX_FILE_ERROR::HEX_RECORD:
		GetHexRecordErrorString( buffer, bufferLength);
		break;
	default:
		_stprintf_s(buffer, bufferLength, _T("Line %d, Unknown error."), m_hexRecordLineNumber);
	}
}

/*
 * Intel
 * Hexadecimal Object File
 * Format Specification
 * Revision A, 1/6/88
 * 
 * 
 * 
 * DISCLAIMER
 * 
 * Intel makes no representation or warranties with respect to the contents
 * hereof and specifically disclaims any implied warranties of
 * merchantability or fitness for any particular purpose.  Further, Intel
 * reserves the right to revise this publication from time to time in the
 * content hereof without obligation of Intel to notify any person of such
 * revision or changes.  The publication of this specification should not
 * be construed as a commitment on Intel's part to implement any product.
 * 
 * 
 * 1. Introduction
 * This document describes the hexadecimal object file format for the Intel
 * 8- bit, 16-bit, and 32-bit microprocessors.  The hexadecimal format is
 * suitable as input to PROM programmers or hardware emulators.
 * Hexadecimal object file format is a way of representing an absolute
 * binary object file in ASCII.  Because the file is in ASCII instead of
 * binary, it is possible to store the file is non-binary medium such as
 * paper-tape, punch cards, etc.; and the file can also be displayed on CRT
 * terminals, line printers, etc..  The 8-bit hexadecimal object file
 * format allows for the placement of code and data within the 16-bit
 * linear address space of the Intel 8-bit processors.  The 16-bit
 * hexadecimal format allows for the 20-bit segmented address space of the
 * Intel 16-bit processors.  And the 32-bit format allows for the 32-bit
 * linear address space of the Intel 32-bit processors.
 * The hexadecimal representation of binary is coded in ASCII alphanumeric
 * characters. For example, the 8-bit binary value 0011-1111 is 3F in
 * hexadecimal.  To code this in ASCII, one 8-bit byte containing the ASCII
 * code for the character '3' (0011-0011 or 033H) and one 8-bit byte
 * containing the ASCII code for the character 'F' (0100-0110 or 046H) are
 * required.  For each byte value, the high-order hexadecimal digit is
 * always the first digit of the pair of hexadecimal digits.  This
 * representation (ASCII hexadecimal) requires twice as ma ny bytes as the
 * binary representation.
 * A hexadecimal object file is blocked into records, each of which
 * contains the record type, length, memory load address and checksum in
 * addition to the data.  There are currently six (6) different types of
 * records that are defined, not all combinations of these records are
 * meaningful, however. The records are:
 * 
 * Data Record (8-, 16-, or 32-bit formats)
 * End of File Record (8-, 16-, or 32-bit formats)
 * Extended Segment Address Record (16- or 32-bit formats)
 * Start Segment Address Record (16- or 32-bit formats)
 * Extended Linear Address Record (32-bit format only)
 * Start Linear Address Record (32-bit format only)
 * 
 * 
 * 2. General Record Format
 * | RECORD  |     LOAD     |         |         |  INFO   |         |
 * |  MARK   |    RECLEN    | OFFSET  | RECTYP  |   or    | CHKSUM  |
 * |  ':'    |              |         |         |  DATA   |         |
 *   1-byte       1-byte      2-bytes   1-byte    n-bytes   1-byte
 * 
 * Each record begins with a RECORD MARK field containing 03AH, the ASCII
 * code for the colon (':') character.
 * Each record has a RECLEN field which specifies the number of bytes of
 * information or data which follows the RECTYP field of the record.  Note
 * that one data byte is represented by two ASCII characters.  The maximum
 * value of the RECLEN field is hexadecimal 'FF' or 255.
 * Each record has a LOAD OFFSET field which specifies the 16-bit starting
 * load offset of the data bytes, therefore this field is only used for
 * Data Records.  In other records where this field is not used, it should
 * be coded as four ASCII zero characters ('0000' or 030303030H).
 * Each record has a RECTYP field which specifies the record type of this
 * record.  The RECTYP field is used to interpret the remaining information
 * within the record.  The encoding for all the current record types are:
 * 
 * '00' Data Record
 * '01' End of File Record
 * '02' Extended Segment Address Record
 * '03' Start Segment Address Record
 * '04' Extended Linear Address Record
 * '05' Start Linear Address Record
 * 
 * Each record has a variable length INFO/DATA field, it consists of zero
 * or more bytes encoded as pairs of hexadecimal digits.  The
 * interpretation of this field depends on the RECTYP field.
 * Each record ends with a CHKSUM field that contains the ASCII hexadecimal
 * representation of the two's complement of  the 8-bit bytes that result
 * from converting each pair of ASCII hexadecimal digits to one byte of
 * binary, from and including the RECLEN field to and including the last
 * byte of the INFO/DATA field.  Therefore, the sum of all the ASCII pairs
 * in a record after converting to binary, from the RECLEN field to and
 * including the CHKSUM field, is zero.
 * 
 * 
 * 3. Extended Linear Address Record    (32-bit format only)
 * | RECORD  |     LOAD     |         |         |         |         |
 * |  MARK   |    RECLEN    | OFFSET  | RECTYP  |  ULBA   | CHKSUM  |
 * |  ':'    |    '02'      | '0000'  |  '04'   |         |         |
 *   1-byte       1-byte      2-bytes   1-byte    2-bytes   1-byte
 * 
 * The 32-bit Extended Linear Address Record is used to specify bits 16-31
 * of the Linear Base Address (LBA), where bits 0-15 of the LBA are zero. 
 * Bits 16-31 of the LBA are referred to as the Upper Linear Base Address
 * (ULBA). The absolute memory address of a content byte in a subsequent
 * Data Record is obtained by adding the LBA to an offset calculated by
 * adding the LOAD OFFSET field of the containing Data Record to the index
 * of the byte in the Data Record (0, 1, 2, ... n).  This offset addition
 * is done modulo 4G (i.e., 32-bits), ignoring any carry, so that offset
 * wrap-around loading (from OFFFFFFFFH to OOOOOOOOOH) results in wrapping
 * around from the end to the beginning of the 4G linear address defined by
 * the LBA.  The linear address at which a particular byte is loaded is
 * calculated as:
 * (LBA + DRLO + DRI) MOD 4G
 * where:
 * DRLO is the LOAD OFFSET field of a Data Record.
 * DRI is the data byte index within the Data Record.
 * 
 * When an Extended Linear Address Record defines the value of LBA, it may
 * appear anywhere within a 32-bit hexadecimal object file. This value
 * remains in effect until another Extended Linear Address Record is
 * encountered.  The LBA defaults to zero until an Extended Linear Address
 * Record is encountered.
 * The contents of the individual fields within the record are:
 * 
 * RECORD MARK
 * This field contains 03AH, the hexadecimal encoding of the ASCII colon
 * (':') character.
 * 
 * RECLEN
 * The field contains 03032H, the hexadecimal encoding of the ASCII
 * characters '02', which is the length, in bytes, of the ULBA data
 * information within this record.
 * 
 * LOAD OFFSET
 * This field contains 030303030H, the hexadecimal encoding of the ASCII
 * characters '0000', since this field is not used for this record.
 * 
 * RECTYP
 * This field contains 03034H, the hexadecimal encoding of the ASCII
 * character '04', which specifies the record type to be an Extended Linear
 * Address Record.
 * 
 * ULBA
 * This field contains four ASCII hexadecimal digits that specify the
 * 16-bit Upper Linear Base Address value.  The high-order byte is the
 * 10th/llth character pair of the record.  The low-order byte is the
 * 12th/13th character pair of the record.
 * 
 * CHKSUM
 * This field contains the check sum on the RECLEN, LOAD OFFSET, RECTYP,
 * and ULBA fields.
 * 
 * 
 * 4. Extended Segment Address Record (16- or 32-bit formats)
 * | RECORD  |     LOAD     |         |         |         |         |
 * |  MARK   |    RECLEN    | OFFSET  | RECTYP  |  USBA   | CHKSUM  |
 * |  ':'    |    '02'      | '0000'  |  '02'   |         |         |
 *   1-byte       1-byte      2-bytes   1-byte    2-bytes   1-byte
 * 
 * The 16-bit Extended Segment Address Record is used to specify bits 4-19
 * of the Segment Base Address (SBA), where bits 0-3 of the SBA are zero. 
 * Bits 4-19 of the SBA are referred to as the Upper Segment Base Address
 * (USBA). The absolute memory address of a content byte in a subsequent
 * Data Record is obtained by adding the SBA to an offset calculated by
 * adding the LOAD OFFSET field of the containing Data Record to the index
 * of the byte in the Data Record (0, 1, 2, ... n).  This offset addition
 * is done modulo 64K (i.e., 16-bits), ignoring any carry, so that offset
 * wraparound loading (from OFFFFH to OOOOOH) results in wrapping around
 * from the end to the beginning of the 64K segment defined by the SBA. 
 * The address at which a particular byte is loaded is calculated as:
 * 
 *         SBA +  ([DRLO  +  DRI]  MOD  64K)
 * 
 * where:
 *         DRLO is the LOAD OFFSET field of a Data Record.
 *         DRI is the data byte index within the Data Record.
 * 
 * When an Extended Segment Address Record defines the value of SBA, it
 * may appear anywhere within a 16-bit hexadecimal object file.  This
 * value remains in effect until another Extended Segment Address
 * Record is encountered.  The SBA defaults to zero until an Extended
 * Segment Address Record is encountered.
 * 
 * The contents of the individual fields within the record are:
 * 
 * RECORD MARK
 * This field contains 03AH, the hexadecimal encoding of the ASCII
 * colon (':') character.
 * 
 * RECLEN
 * The field contains 03032H, the hexadecimal encoding of the ASCII
 * characters '02', which is the length, in bytes, of the USBA data
 * information within this record.
 * 
 * LOAD OFFSET
 * This field contains 030303030H, the hexadecimal encoding of the
 * ASCII characters '0000', since this field is not used for this
 * record.
 * 
 * RECTYP
 * This field contains 03032H, the hexadecimal encoding of the ASCII
 * character '02', which specifies the record type to be an Extended
 * Segment Address Record.
 * 
 * USBA
 * This field contains four ASCII hexadecimal digits that specify the
 * 16-bit Upper Segment Base Address value.  The high-order byte is the
 * 10th/1lth character pair of the record.  The low-order byte is the
 * 12th/13th character pair of the record.
 * 
 * CHKSUM
 * This field contains the check sum on the RECLEN, LOAD OFFSET,
 * RECTYP, and USBA fields.
 * 
 * 5.    Data Record   (8-, 16-, or 32-bit formats)
 * 
 * | RECORD  |     LOAD     |         |         |         |         |
 * |  MARK   |    RECLEN    | OFFSET  | RECTYP  |  DATA   | CHKSUM  |
 * |  ':'    |              |         |  '00'   |         |         |
 *  1-byte       1-byte      2-bytes   1-byte    n-bytes   1-byte
 * 
 * 
 * The Data Record provides a set of hexadecimal digits that represent
 * the ASCII code for data bytes that make up a portion of a memory
 * image.  The method for calculating the absolute address (linear in
 * the 8-bit and 32-bit case and segmented in the 16-bit case) for each
 * byte of data is described in the discussions of the Extended Linear
 * Address Record and the Extended Segment Address Record.
 * 
 * The contents of the individual fields within the record are:
 * 
 * RECORD MARK
 * This field contains 03AH, the hexadecimal encoding of the ASCII
 * colon (':') character.
 * 
 * RECLEN
 * The field contains two ASCII hexadecimal digits that specify the
 * number of data bytes in the record.  The maximum value is 'FF' or
 * 04646H (255 decimal).
 * 
 * LOAD OFFSET
 * This field contains four ASCII hexadecimal digits representing the
 * offset from the LBA (see Extended Linear Address Record) or SBA (see
 * Extended Segment Address Record) defining the address which the
 * first byte of the data is to be placed.
 * 
 * RECTYP
 * This field contains 03030H, the hexadecimal encoding of the ASCII
 * character '00', which specifies the record type to be a Data Record.
 * 
 * DATA
 * This field contains pairs of ASCII hexadecimal digits, one pair for
 * each data byte.
 * 
 * CHKSUM
 * This field contains the check sum on the RECLEN, LOAD OFFSET,
 * RECTYP, and DATA fields.
 * 
 * 
 * 6.    Start Linear Address Record  (32-bit format only)
 * 
 * | RECORD  |     LOAD     |         |         |         |         |
 * |  MARK   |    RECLEN    | OFFSET  | RECTYP  |  EIP    | CHKSUM  |
 * |  ':'    |    '04'      | '0000'  |  '05'   |         |         |
 *   1-byte       1-byte      2-bytes   1-byte    4-bytes   1-byte
 * 
 * 
 * The Start Linear Address Record is used to specify the execution
 * start address for the object file.  The value given is the 32-bit
 * linear address for the EIP register.  Note that this record only
 * specifies the code address within the 32-bit linear address space of
 * the 80386.  If the code is to start execution in the real mode of
 * the 80386, then the Start Segment Address Record should be used
 * instead, since that record specifies both the CS and IP register
 * contents necessary for real mode.
 * 
 * The Start Linear Address Record can appear anywhere in a 32-bit
 * hexadecimal object file.  If such a record is not present in a
 * hexadecimal object file, a loader is free to assign a default start
 * address.
 * 
 * The contents of the individual fields within the record are:
 * 
 * RECORD MARK
 * This field contains 03AH, the hexadecimal encoding of the ASCII
 * colon (':') character.
 * 
 * RECLEN
 * The field contains 03034H, the hexadecimal encoding of the ASCII
 * characters '04', which is the length, in bytes, of the EIP register
 * content within this record.
 * 
 * LOAD OFFSET
 * This field contains 030303030H, the hexadecimal encoding of the
 * ASCII characters '0000', since this field is not used for this
 * record.
 * 
 * RECTYP
 * This field contains 03035H, the hexadecimal encoding of the ASCII
 * character '05', which specifies the record type to be a Start Linear
 * Address Record.
 * 
 * EIP
 * This field contains eight ASCII hexadecimal digits that specify the
 * 32-bit EIP register contents.  The high-order byte is the 10th/1lth
 * character pair.
 * 
 * CHKSUM
 * This field contains the check sum on the RECLEN, LOAD OFFSET,
 * RECTYP, and EIP fields.
 * 
 * 
 * 7.    Start Segment Address Record (16- or 32-bit formats)
 * 
 * | RECORD  |     LOAD     |         |         |         |         |
 * |  MARK   |    RECLEN    | OFFSET  | RECTYP  |  CS/IP  | CHKSUM  |
 * |  ':'    |    '04'      | '0000'  |  '03'   |         |         |
 *   1-byte       1-byte      2-bytes   1-byte    4-bytes   1-byte
 * 
 * 
 * The Start Segment Address Record is used to specify the execution
 * start address for the object file.  The value given is the 20-bit
 * segment address for the CS and IP registers.  Note that this record
 * only specifies the code address within the 20-bit segmented address
 * space of the 8086/80186.
 * 
 * The Start Segment Address Record can appear anywhere in a 16-bit
 * hexadecimal object file.   If such a record is not present in a
 * hexadecimal object file, a loader is free to assign a default start
 * address.
 * 
 * The contents of the individual fields within the record are:
 * 
 * RECORD MARK
 * This field contains 03AH, the hexadecimal encoding of the ASCII
 * colon (':') character.
 * 
 * RECLEN
 * The field contains 03034H, the hexadecimal encoding of the ASCII
 * characters '04', which is the length, in bytes, of the CS/IP
 * register contents within this record.
 * 
 * LOAD OFFSET
 * This field contains 030303030H, the hexadecimal encoding of the
 * ASCII characters '0000', since this field is not used for this
 * record.
 * 
 * RECTYP
 * This field contains 03033H, the hexadecimal encoding of the ASCII
 * character '03', which specifies the record type to be a Start
 * Segment Address Record.
 * 
 * CS/IP
 * This field contains eight ASCII hexadecimal digits that specify the
 * 16-bit CS register and 16-bit IP register contents.  The high-order
 * byte of the CS register content is the 10th/llth character pair, the
 * low-order byte is the 12th/13th character pair of the record.  The
 * high-order byte of the IP register content is the 14th/15th
 * character pair, the low-order byte is the 16th/17th character pair
 * of the record.
 * 
 * CHKSUM
 * This field contains the check sum on the RECLEN, LOAD OFFSET,
 * RECTYP, and CS/IP fields.
 * 
 * 
 * 
 * 8.   End of File Record  (8-, 16-, or 32-bit formats)
 * 
 * | RECORD  |     LOAD     |         |         |         |
 * |  MARK   |    RECLEN    | OFFSET  | RECTYP  | CHKSUM  |
 * |  ':'    |    '00'      | '0000'  |  '01'   |         |
 *   1-byte       1-byte      2-bytes   1-byte    1-byte
 * 
 * The End of File Record specifies the end of the hexadecimal object
 * file.
 * 
 * The contents of the individual fields within the record are:
 * 
 * RECORD  MARK
 * This field contains 03AH, the hexadecimal encoding of the ASCII
 * colon (':') character.
 * 
 * RECLEN
 * The field contains 03030H, the hexadecimal encoding of the ASCII
 * characters '00'.  Since this record does not contain any INFO/DATA
 * bytes, the length is zero.
 * 
 * LOAD  OFFSET
 * This field contains 030303030H, the hexadecimal encoding of the
 * ASCII characters '0000', since this field is not used for this
 * record.
 * 
 * RECTYP
 * This field contains 03031H, the hexadecimal encoding of the ASCII
 * character '01', which specifies the record type to be an End of File
 * Record.
 * 
 * CHKSUM
 * This field contains the check sum an the RECLEN, LOAD OFFSET, and
 * RECTYP fields.  Since all the fields are static, the check sum can
 * also be calculated statically, and the value is 04646H, the
 * hexadecimal encoding of the ASCII characters 'FF'.
 * 
 * ========================================================================
 * END
 * 
 */
