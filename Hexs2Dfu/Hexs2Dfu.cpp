/*******************************************************************************
 * Copyright (c) 2019 Apple Enterprises. http://www.applehome.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
 
// Hexs2Dfu.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include "pch.h"
#include "stdio.h"
#include "stdlib.h"
#include "IntelHexRecords.h"
#include "DFUFile.h"

void SetWorkingDirectory(TCHAR* moduleName);
bool GetParameters(int argc, TCHAR* argv[]);
bool ParseCommandLine(int argc, TCHAR* argv[]);
void PrintHelp();

#define TEMPORARY_BUFFER_SIZE 0x10000
uint8_t gBuffer[TEMPORARY_BUFFER_SIZE];

#define ERROR_MESSAGE_SIZE 1024
TCHAR gErrorMessage[ERROR_MESSAGE_SIZE];

#define CONFIG_FILE_BUFFER_SIZE 4096
TCHAR gConfigFileBuffer[CONFIG_FILE_BUFFER_SIZE];
#define MAXIMUM_CONFIF_FILE_ARGC 100
uint32_t gConfigFileArgc;
TCHAR* gConfigFileArgv[MAXIMUM_CONFIF_FILE_ARGC];

#define MAXIMUM_NUMBER_OF_INPUT_FILES 100
uint32_t gNumberOfInputFileNames = 0;
TCHAR* gInputFileNames[MAXIMUM_NUMBER_OF_INPUT_FILES];
TCHAR* gInputTargetNames[MAXIMUM_NUMBER_OF_INPUT_FILES];
uint32_t gInputTargetOffsets[MAXIMUM_NUMBER_OF_INPUT_FILES];
TCHAR gInputFileName[_MAX_PATH + 1];

uint32_t gNumberOfOutputFileNames = 0;
TCHAR gOutputFileName[_MAX_PATH+1];

TCHAR gConfigFileName[_MAX_PATH + 1];


uint32_t gBcd = 0xffff;
uint32_t gPid = 0xdf11;
uint32_t gVid = 0x0483;

int _tmain(int argc, TCHAR* argv[])
{
	SetWorkingDirectory(argv[0]);

	bool result = GetParameters(argc, argv);
	if (!result) {
		if (gErrorMessage[0] != '\0') {
			_tprintf(gErrorMessage);
		}
		PrintHelp();
		return 1;
	}

	CDFUFile dfuFile;
	result = dfuFile.OpenDfuFile(gOutputFileName);
	if (result == false) {
		_tprintf(_T("Failed to open \"%s\""), gOutputFileName);
		PrintHelp();
		return 1;
	}

	CIntelHexRecords hexFile;

	for (uint32_t index = 0; index < gNumberOfInputFileNames; index++) {
		if (_tfullpath(gInputFileName, gInputFileNames[index], _MAX_PATH) == NULL) {
			_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: Invalid input file name \"%s\".\r\n\r\n"), gInputFileNames[index]);
			PrintHelp();
			return 1;
		}
		result = hexFile.OpenHexFile(gInputFileName);
		if (result == false) {
			_tprintf(_T("Failed to open \"%s\""), gInputFileName);
			PrintHelp();
			return 1;
		}

		// find out information about file
		result = hexFile.ReadFile();
		if (result == false) {
			hexFile.GetHexFileErrorString(gErrorMessage, ERROR_MESSAGE_SIZE);
			_tprintf(_T("%s (%s)\r\n"), gErrorMessage, gInputFileName);
			hexFile.CloseHexFile();
			dfuFile.CloseDfuFile();
			return 1;
		}

		dfuFile.StartDfuImage();

		uint32_t numberOfExtendedLinearAddress = hexFile.GetNumberOfBlocks();
		for (uint32_t blockIndex = 0; blockIndex < numberOfExtendedLinearAddress; blockIndex++) {
			bool result = hexFile.ReadBlock(blockIndex, gBuffer, TEMPORARY_BUFFER_SIZE);
			if (result == false) {
				hexFile.GetHexFileErrorString(gErrorMessage, ERROR_MESSAGE_SIZE);
				_tprintf(_T("%s (%s)\r\n"), gErrorMessage, gInputFileName);
				hexFile.CloseHexFile();
				dfuFile.CloseDfuFile();
				return 1;
			}
			uint32_t extendedAddress;
			uint32_t minimumBlockOffset;
			uint32_t maximumBlockOffset;
			bool blockHasData = hexFile.GetBlockAddresses(extendedAddress, minimumBlockOffset, maximumBlockOffset);
			if (blockHasData) {
				uint32_t elementSize = maximumBlockOffset - minimumBlockOffset + 1;
				uint32_t elementAddress = extendedAddress + minimumBlockOffset;
				uint8_t * dataBuffer = gBuffer + minimumBlockOffset;
				dfuFile.AddDfuImageElement(elementAddress, elementSize, dataBuffer);
			}
		}

		// if no target name, create one using its index
		if (gInputTargetNames[index] == NULL) {
			_stprintf_s((TCHAR*)gBuffer, TEMPORARY_BUFFER_SIZE/sizeof(TCHAR), _T("PCB %d"), index);
			gInputTargetNames[index] = (TCHAR*)gBuffer;
		}
		dfuFile.EndDfuImage(gInputTargetNames[index], gInputTargetOffsets[index]);

		hexFile.CloseHexFile();
	}

	dfuFile.EndDfuFile(gBuffer, TEMPORARY_BUFFER_SIZE, gBcd, gPid, gVid);
	dfuFile.CloseDfuFile();

	_tprintf(_T("Created %s'.\r\n\r\n"), gOutputFileName);

	return 0;
}


uint32_t ConvertStringToNumber(TCHAR* parameter)
{
	uint32_t value;

	if ((*parameter == '0') && ((*(parameter+1) == 'x') || (*(parameter + 1) == 'X'))) {
		parameter += 2;
		_stscanf_s(parameter, _T("%x"), &value);		
	}
	else {
		_stscanf_s(parameter, _T("%d"), &value);
	}
	return value;
}

bool ReadConfigurationOptionsFromFile(LPCTSTR fileName)
{
	FILE* m_file;
	errno_t error = _tfopen_s(&m_file, fileName, _T("rt"));
	if (error != 0) {
		_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: Could not open option file \"%s\".\r\n\r\n"), fileName);
		return false;
	}

	// read file into buffer
	bool fileError = false;
	TCHAR * buffer = gConfigFileBuffer;
	uint32_t bufferLength = CONFIG_FILE_BUFFER_SIZE;
	for (;;) {
		TCHAR * getResult = _fgetts(buffer, bufferLength, m_file);
		if (getResult == NULL) {
			if (ferror(m_file)) {
				fileError = true;
				break;
			}
			if (feof(m_file) != 0) {
				// end of file
				break;
			}
			fileError = true;
			break;
		}

		uint32_t lineLength = _tcslen(buffer);
		if (buffer[lineLength - 1] == '\n') {
			buffer[lineLength - 1] = ' ';
		}
		bufferLength -= lineLength;
		buffer += lineLength;
	}

	fclose(m_file);

	if (fileError) {
		return false;
	}

	uint32_t charsRead = _tcslen(gConfigFileBuffer);

	gConfigFileArgc = 1;
	gConfigFileArgv[gConfigFileArgc] = &buffer[0];
	bool foundLineStart = false;
	bool insideQuotes = false;
	buffer = gConfigFileBuffer;
	for (uint32_t i = 0; i < charsRead; i++) {
		if (buffer[i] == '"') {
			if (!insideQuotes) {
				insideQuotes = true;
			}
			else {
				buffer[i] = '\0';
				insideQuotes = false;
			}
		}
		else if (buffer[i] != ' ') {
			if (!foundLineStart) {
				gConfigFileArgv[gConfigFileArgc] = &buffer[i];
				foundLineStart = true;
			}
		}
		else {
			if (foundLineStart && !insideQuotes) {
				buffer[i] = '\0';
				gConfigFileArgc++;
				foundLineStart = false;
			}
		}
	}

	return true;
}


enum HEXTODFU_OPTION {
	HEXTODFU_OPTION_BCD,
	HEXTODFU_OPTION_FILE_PARAMETERS,
	HEXTODFU_OPTION_HELP,
	HEXTODFU_OPTION_INPUT_FILE,
	HEXTODFU_OPTION_OUTPUT_FILE,
	HEXTODFU_OPTION_ADDRESS_OFFSET,
	HEXTODFU_OPTION_PID,
	HEXTODFU_OPTION_TARGET_NAME,
	HEXTODFU_OPTION_VID,
	HEXTODFU_OPTION_FINISHED,
	HEXTODFU_OPTION_ERROR,
};



HEXTODFU_OPTION GetNextOption(int argc, TCHAR* argv[], int &parameterIndex, TCHAR* &parameter)
{
	// if parsed all options, then done
	if (parameterIndex >= argc) {
		return HEXTODFU_OPTION_FINISHED;
	}

	// make sure option starts with dash
	TCHAR * parameterString = argv[parameterIndex];
	if (*parameterString != '-') {
		_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: Missing '-' \"%s\".\r\n\r\n"), parameterString);
		return HEXTODFU_OPTION_ERROR;
	}
	parameterString++;

	// get char for option
	TCHAR option = *parameterString++;

	if (option == 'h' || option == 'H' || option == '?') {
		return HEXTODFU_OPTION_HELP;
	}

	// check if argument is concatenated with option 
	if (*parameterString == '\0') {
		// use next string as parameter 
		parameterIndex++;
		if (parameterIndex >= argc) {
			_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: Missing parameter for option '%c'.\r\n\r\n"), option);
			return HEXTODFU_OPTION_ERROR;
		}
		parameterString = argv[parameterIndex];
	}

	parameter = parameterString;
	parameterIndex++;

	switch (option) {
	case 'b': return HEXTODFU_OPTION_BCD;
	case 'f': return HEXTODFU_OPTION_FILE_PARAMETERS;
	case 'i': return HEXTODFU_OPTION_INPUT_FILE;
	case 'o': return HEXTODFU_OPTION_OUTPUT_FILE;
	case 'O': return HEXTODFU_OPTION_ADDRESS_OFFSET;
	case 'p': return HEXTODFU_OPTION_PID;
	case 'n': return HEXTODFU_OPTION_TARGET_NAME;
	case 'v': return HEXTODFU_OPTION_VID;
	default: 
		_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: Unknown option '%c'.\r\n\r\n"), option);
		return HEXTODFU_OPTION_ERROR;
	}
}


bool ParseCommandLine(int argc, TCHAR* argv[])
{
	int parameterIndex = 1;
	TCHAR* parameter;
	for (;;) {
		HEXTODFU_OPTION option = GetNextOption(argc, argv, parameterIndex, parameter);
		switch (option) {
		case HEXTODFU_OPTION_BCD:
			gBcd = ConvertStringToNumber(parameter);
			break;

		case HEXTODFU_OPTION_FILE_PARAMETERS:
			if (_tfullpath(gConfigFileName, parameter, _MAX_PATH) == NULL) {
				_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: Invalid configuration file name \"%s\".\r\n\r\n"), parameter);
				return false;
			}
			if (!ReadConfigurationOptionsFromFile(gConfigFileName)) {
				return false;
			}
			if (!ParseCommandLine(gConfigFileArgc, gConfigFileArgv)) {
				return false;
			}
			break;

		case HEXTODFU_OPTION_HELP:
			return false;

		case HEXTODFU_OPTION_INPUT_FILE:
			gInputFileNames[gNumberOfInputFileNames] = parameter;
			gInputTargetNames[gNumberOfInputFileNames] = NULL;
			gInputTargetOffsets[gNumberOfInputFileNames] = 0;
			gNumberOfInputFileNames++;
			break;

		case HEXTODFU_OPTION_OUTPUT_FILE:
			if (_tfullpath(gOutputFileName, parameter, _MAX_PATH) == NULL) {
				_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: Invalid output file name \"%s\".\r\n\r\n"), parameter);
				return false;
			}
			gNumberOfOutputFileNames++;
			break;

		case HEXTODFU_OPTION_ADDRESS_OFFSET:
			if (gNumberOfInputFileNames == 0) {
				return false;
			}
			gInputTargetOffsets[gNumberOfInputFileNames - 1] = ConvertStringToNumber(parameter);
			break;

		case HEXTODFU_OPTION_PID:
			gPid = ConvertStringToNumber(parameter);
			break;

		case HEXTODFU_OPTION_TARGET_NAME:
			if (gNumberOfInputFileNames == 0) {
				return false;
			}
			gInputTargetNames[gNumberOfInputFileNames-1] = parameter;
			break;

		case HEXTODFU_OPTION_VID:
			gVid = ConvertStringToNumber(parameter);
			break;

		case HEXTODFU_OPTION_ERROR:
			return false;

		case HEXTODFU_OPTION_FINISHED:
			return true;
		}
	}

	return true;
}

bool GetParameters(int argc, TCHAR* argv[])
{
	gErrorMessage[0] = '\0';

	// if no argumnets , retunr help
	if (argc == 1) {
		return false;
	}

	if (argc == 2) {
		TCHAR option = *argv[1];
		if (option == 'h' || option == 'H' || option == '?') {
			return false;
		}
	}

	bool result = ParseCommandLine(argc, argv);
	if (!result) {
		return false;
	}

	if (gNumberOfInputFileNames == 0) {
		_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: No HEX input file specified.\r\n\r\n"));
		return false;
	}

	if (gNumberOfOutputFileNames == 0) {
		_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: No DFU output file specified.\r\n\r\n"));
		return false;
	}

	if (gNumberOfOutputFileNames > 1) {
		_stprintf_s(gErrorMessage, ERROR_MESSAGE_SIZE, _T("Error: More than one DFU output file specified.\r\n\r\n"));
		return false;
	}

	return true;
}

void SetWorkingDirectory(TCHAR* moduleName)
{
	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR filename[_MAX_FNAME];
	TCHAR extension[_MAX_EXT];
	TCHAR exeDirectory[_MAX_PATH];

	_tsplitpath_s(moduleName, drive, dir, filename, extension);
	_tmakepath_s(exeDirectory, drive, dir, NULL, NULL);
	_tprintf(_T("Exe Directory \"%s\"\r\n"), (TCHAR*)exeDirectory);

	_tchdir(exeDirectory);

	_tgetcwd(exeDirectory, _MAX_PATH / sizeof(TCHAR));
	_tprintf(_T("Working Directory \"%s\"\r\n"), (TCHAR*)exeDirectory);
}


void PrintHelp() {
	_tprintf(_T("STM32 Hexs2Dfu version 0.1.0\r\n"));
	_tprintf(_T("(c)) Apple Enterprises 2019\r\n"));
	_tprintf(_T("Combines multiple HEX Files into a single DFU file\r\n"));
	_tprintf(_T("Options:\r\n"));
	_tprintf(_T("-b        - firmware version number (optional, default: 0xFFFF))\r\n"));
	_tprintf(_T("-f        - file name containing options (optional)\r\n"));
	_tprintf(_T("-h        - help\r\n"));
	_tprintf(_T("-i        - input HEX file name (mandatory))\r\n"));
	_tprintf(_T("-n        - input name (optional, default: PCB x))\r\n"));
	_tprintf(_T("-o        - output DFU file name (mandatory))\r\n"));
	_tprintf(_T("-O        - offset to be added to firmware address (optional)\r\n"));
	_tprintf(_T("-p        - USB Pid (optional, default: 0xDF11))\r\n"));
	_tprintf(_T("-v        - USB Vid (optional, default: 0x0483))\r\n"));
	_tprintf(_T("Example 1: hexs2dfu -i infile.hex -i outfile.dfu\r\n"));
	_tprintf(_T("Example 2: hexs2dfu -f options.txt\r\n"));
	_tprintf(_T("contents of options.txt\r\n"));
	_tprintf(_T("-i \"input1.hex\" -n \"Main PCB\" -O 0\r\n"));
	_tprintf(_T("-i \"input2.hex\" -n \"Daughter 1 PCB\" -O 0x80000\r\n"));
	_tprintf(_T("-i \"input3.hex\" -n \"Daughter 2 PCB\" -O 0x90000\r\n"));
	_tprintf(_T("-o \"output.dfu\"\r\n"));
	_tprintf(_T("-b 0x1234 -p 0xDF11 -v 0x0483\r\n"));
}
