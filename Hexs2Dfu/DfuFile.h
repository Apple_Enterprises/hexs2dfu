/*******************************************************************************
 * Copyright (c) 2019 Apple Enterprises. http://www.applehome.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#ifndef __DFUFILE_H__
#define __DFUFILE_H__

#include "stdint.h"
#include "stdbool.h"

enum class DFU_FILE_ERROR : int8_t {
	ERROR_NONE,
	SEEK_FAIL,
	READ_FAIL,
	WRITE_FAIL,
	TOTAL
};

#define PREFIX_SIGNATURE_SIZE	5
#define PREFIX_SIGNATURE		"DfuSe"
#define PREFIX_VERSION			1

#define TARGET_PREFIX_SIGNATURE_SIZE	6
#define TARGET_PREFIX_SIGNATURE		"Target"

#pragma pack(push)
#pragma pack(1)
typedef struct
{
	char	szSignature[PREFIX_SIGNATURE_SIZE];
	uint8_t	bVersion;
	uint32_t	dwImageSize;
	uint8_t	bTargets;
} DFUPREFIX;

typedef struct
{
	char	szSignature[TARGET_PREFIX_SIGNATURE_SIZE];
	uint8_t	bAlternateSetting;
	uint32_t	bTargetNamed;
	char	szTargetName[251];	// if bTargetNamed != 2, the size is 255 and dwTargetOffset does not exits 
	uint32_t	dwTargetOffset;
	uint32_t	dwTargetSize;
	uint32_t	dwNbElements;
} TARGETPREFIX;


typedef struct
{
	uint32_t	dwElementAddress;
	uint32_t	dwElementSize;
	//	uint8_t	Data[0];
} ELEMENT;

typedef struct
{
	uint8_t bcdDeviceLo;
	uint8_t bcdDeviceHi;
	uint8_t idProductLo;
	uint8_t idProductHi;
	uint8_t idVendorLo;
	uint8_t idVendorHi;
	uint8_t bcdDFULo;		// 0x1a
	uint8_t bcdDFUHi;		// 0x01
	char ucDfuSignature[3];
	uint8_t bLength;
	uint8_t dwCRC[4];
} DFUSUFFIX;
#pragma pack(pop)

class CDFUFile 
{
public:
	CDFUFile();
	virtual ~CDFUFile();

	bool OpenDfuFile(LPCTSTR fileName);
	void CloseDfuFile();

	bool StartDfuImage();
	bool AddDfuImageElement(uint32_t elementAddress, uint32_t elementSize, uint8_t * buffer);
	bool EndDfuImage(TCHAR *targetName, uint32_t targetOffset);
	bool EndDfuFile(uint8_t * temporaryBuffer, uint32_t temporaryBufferSize, uint16_t bcd, uint16_t pid, uint16_t vid);

protected:
	bool WriteDfuPrefix();
	bool WriteTargetPrefix(TCHAR *targetName, uint32_t targetOffset);
	bool WriteDfuSuffix(uint8_t * temporaryBuffer, uint32_t temporaryBufferSize, uint16_t bcd, uint16_t pid, uint16_t vid );
	uint32_t CalculateCRC(uint8_t * temporaryBuffer, uint32_t temporaryBufferSize);

private:
	FILE*	m_file;
	DFU_FILE_ERROR m_errorCode;

	// prefix parameters
	DFUPREFIX m_prefix;
	uint32_t m_imageSize;
	uint32_t m_numberOfDfuImages;

	// target prefix parameters
	TARGETPREFIX m_targetPrefix;
	uint32_t m_targetPrefixLocation;
	uint32_t m_targetSize;

	// element parameters
	ELEMENT m_element;
	uint32_t m_numberOfElements;
//	uint32_t m_elementLocation;

	// suffix parameters
	DFUSUFFIX m_suffix;
};

#endif