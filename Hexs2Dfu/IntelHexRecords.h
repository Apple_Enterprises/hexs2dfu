/*******************************************************************************
 * Copyright (c) 2019 Apple Enterprises. http://www.applehome.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files(the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and /or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/

#if !defined(__INTELHEXRECORDS_H__)
#define __INTELHEXRECORDS_H__

#include "stdint.h"
#include "stdbool.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


enum class HEX_FILE_ERROR : int8_t {
	ERROR_NONE,
	SEEK_FAIL,
	READ_FAIL,
	HEX_RECORD,
	TOTAL
};

enum class HEX_RECORD_ERROR : int8_t {
	NONE,
	NO_STARTING_COLON,
	BAD_CHAR_IN_RECORD_LENGTH,
	BAD_CHAR_IN_RECORD_OFFSET,
	BAD_CHAR_IN_RECORD_TYPE,
	INVALID_RECORD_LENGTH,
	WRONG_SIZE_RECORD_LENGTH,
	BAD_CHAR_IN_DATA,
	BAD_CHAR_IN_CHECKSUM,
	NON_MATCHING_CHECKSUM,
	NOT_IMPLEMENTED_RECORD,
	TOTAL
};

enum class HEX_RECORD_NUMBER : int8_t {
	ERROR = -1,
	DATA = 0,
	END_OF_FILE,
	EXTENDED_SEGMENT_ADDRESS,
	START_SEGMENT_ADDRESS,
	EXTENDED_LINEAR_ADDRESS,
	START_LINEAR_ADDRESS,
	TOTAL
};

class CIntelHexRecords  
{
public:
	CIntelHexRecords();
	virtual ~CIntelHexRecords();

	// Operations
	bool OpenHexFile(LPCTSTR fileName);
	void CloseHexFile();

	bool ReadFile();
	void GetHexFileErrorString(TCHAR * buffer, uint32_t bufferLength);

	uint32_t GetNumberOfBlocks() { return m_numberOfExtendedLinearAddress; }
	bool ReadBlock(int32_t blockIndex, uint8_t * buffer, uint32_t bufferLength);
	bool GetBlockAddresses(uint32_t &extendedAddress, uint32_t &minimumBlockOffset, uint32_t &maximumBlockOffset);

protected:
	void ExamineBlock(uint8_t * buffer, int32_t bufferLength);
	HEX_RECORD_NUMBER GetRecordNumber(LPCTSTR hexRecord);
	bool ReadHexRecord();
	int HexConvert(TCHAR c);
	int GetHexByte(LPCTSTR p);
	int GetHexWord(LPCTSTR p);
	uint32_t GetHexDWord(LPCTSTR p);
	char GetChar(LPCTSTR p);
	void GetHexRecordErrorString(TCHAR * buffer, uint32_t bufferLength);

private:
	FILE*	m_file;
	HEX_FILE_ERROR m_errorCode;

	uint32_t m_extendedAddress;
	int32_t m_minimumOffset;
	int32_t m_maximumOffset;

	TCHAR  m_hexRecord[512];
	uint32_t m_hexRecordLineNumber;
	HEX_RECORD_ERROR m_hexRecordError;

	//  HEX_RECORD_NUMBER::DATA variables
	uint32_t m_hexRecordDataAddress;
	uint8_t m_hexRecordData[40];
	uint32_t m_hexRecordDataSize;

	//  HEX_RECORD_NUMBER::EXTENDED_LINEAR_ADDRESS variables
	uint32_t m_hexRecordExtendedLinearAddress; // current extend linear address being used - set by record type 4
	int32_t m_numberOfExtendedLinearAddress; // current extend linear address being used - set by record type 4

	//  HEX_RECORD_NUMBER::START_LINEAR_ADDRESS variables
	uint32_t m_hexRecordStartLinearAddress; // current extend linear address being used - set by record type 4
};

#endif // !defined(__INTELHEXRECORDS_H__)
